# Iloot

## About

Iloot is intended for use in any Minecraft world involving custom loot tables, from events, structures, entities, to all else which requires them. With another structure mod, namely '[CubicStruct](https://gitlab.com/Absthistle/cubicstruct),' (Cubic Chunks only) or any similar mod, it can also be implemented to automatically paste custom loot tables into world save files, and therefore permitting the players to make use of said tables without hassle. Using this mod allows the player to create loot tables which appear in the world by default, provided any structure or event loads them.

## Usage

Upon initializing the mod for the first time, the mod will generate the directory 'iloot_tables' within the main Minecraft folder. For loot tables to be used in the world, paste them into the corresponding subdirectory (chests, entities, gameplay) and load them with `{LootTable:"iloot:type/loot_table"}`.

For example, loading a loot table named 'example.json' into a chest could be done using 

> /give @p minecraft:chest 1 0 {BlockEntityTag:{LootTable:"iloot:chests/example"}}