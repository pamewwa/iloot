package mod.puradox.iloot;

import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Mod.EventBusSubscriber
public class LootGen {

    public static File worldLootDir;

    public static File worldChestLootDir;
    public static File worldEntityLootDir;
    public static File worldGameLootDir;

    @SubscribeEvent
    public static void onLoad(WorldEvent.Save event) throws IOException {
        worldLootDir = new File(event.getWorld().getSaveHandler().getWorldDirectory() + "/data/loot_tables/iloot/");

        worldChestLootDir = new File(worldLootDir + "/chests/");
        worldEntityLootDir = new File(worldLootDir + "/entities/");
        worldGameLootDir = new File(worldLootDir + "/gameplay/");
        //Set all directories.


        if(!worldLootDir.exists()) {
            if(!new File(worldLootDir.getParent()).exists()) {
                Files.createDirectory(Paths.get(worldLootDir.getParent()));
            }
            Files.createDirectory(Paths.get(String.valueOf(worldLootDir)));
        }
        if(!worldChestLootDir.exists()) {Files.createDirectory(Paths.get(String.valueOf(worldChestLootDir)));}
        if(!worldEntityLootDir.exists()) {Files.createDirectory(Paths.get(String.valueOf(worldEntityLootDir)));}
        if(!worldGameLootDir.exists()) {Files.createDirectory(Paths.get(String.valueOf(worldGameLootDir)));}
        //Initialize world loot table directories.
        FileUtils.copyDirectory(Iloot.chestLootDir, worldChestLootDir);
        FileUtils.copyDirectory(Iloot.entityLootDir, worldEntityLootDir);
        FileUtils.copyDirectory(Iloot.gameLootDir, worldGameLootDir);
        //Copy everything from Iloot to the world loot table directory.
    }
}
